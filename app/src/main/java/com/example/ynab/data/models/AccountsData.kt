package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class AccountsData (@SerializedName("accounts")var accounts: ArrayList<AccountEntity>)
