package com.example.ynab.data.models

data class NewAccount(val name: String, val balance: Double, val type: String)