package com.example.ynab.data.models

data class Account(
    var id: String,
    var name: String,
    var type: String,
    var on_budget: Boolean,
    var closed: Boolean,
    var beautifiedBalance: String,
    var note: String? = null,
    var beautifiedClearedBalance: String,
    var beautifiedUnclearedBalance: String,
    var transeferPayeeId: String
) {
}