package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

class BudgetsData(
    @SerializedName("budgets") var budgets: List<Budget>,
    @SerializedName("default_budget") var defaultBudget: Budget? = null
)