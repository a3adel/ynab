package com.example.ynab.data

import com.example.ynab.data.models.*


interface RemoteDataSource {
    suspend fun getBudgets(): Resource<BudgetsData>
    suspend fun getBudgetAccounts(budgetId: String): Resource<AccountsData>
    suspend fun addAccount(budgetId:String,createAccountEntity: CreateAccountRequestWrapper):Resource<AddAccountResponse>
}