package com.example.ynab.data.error

interface ErrorFactory {
    fun getError(errorCode: Int): Error
}
