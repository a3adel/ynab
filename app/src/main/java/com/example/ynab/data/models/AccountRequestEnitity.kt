package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName
data class AccountRequestEnitity(
    @SerializedName("name") var name: String,
    @SerializedName("type") var type: String,
    @SerializedName("balance") var balance: Int
) {
}