package com.example.ynab.data.repos

import com.example.ynab.data.Resource
import com.example.ynab.data.models.*
import kotlinx.coroutines.flow.Flow

interface AccountRepositorySource {
    suspend fun getBudgetAccounts(budgetId: Budget): Flow<Resource<ArrayList<Account>>>
    suspend fun createNewAccount(budget:Budget,account:NewAccount):Flow<Resource<Account>?>
}