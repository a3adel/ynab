package com.example.ynab.data

import com.example.ynab.data.models.AccountsResponse
import com.example.ynab.data.models.AddAccountResponse
import com.example.ynab.data.models.BudgetsResponse
import com.example.ynab.data.models.CreateAccountRequestWrapper
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface YNABNetworkServices {
    @GET("/v1/budgets")
    suspend fun getBudgets(): Response<BudgetsResponse>

    @GET("/v1/budgets/{budget_id}/accounts")
    suspend fun getBudgetAccounts(@Path("budget_id") id: String): Response<AccountsResponse>

    @POST("/v1/budgets/{budget_id}/accounts")
    suspend fun createAccount(
        @Path("budget_id") id: String,
        @Body createAccount: CreateAccountRequestWrapper
    ): Response<AddAccountResponse>
}