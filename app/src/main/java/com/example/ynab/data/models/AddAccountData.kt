package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class AddAccountData(@SerializedName("account") var account: AccountEntity)