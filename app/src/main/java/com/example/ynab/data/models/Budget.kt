package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.math.min

data class Budget(
    @SerializedName("id") var id: String,
    @SerializedName("name") var name: String,
    @SerializedName("last_modified_on") var lastModify: Date,
    @SerializedName("first_month") var firstMonth: Date,
    @SerializedName("last_month") var lastMonth: Date,
    @SerializedName("date_format") var dateFormat: APIDateFormat,
    @SerializedName("currency_format") var currencyFormat: CurrencyFormat
) {
    fun getLastModified():String{
        val currentDate = Date()

        val diff: Long = currentDate.time - lastModify.getTime()
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24
        val months = days/30
        val years = months/12
        var lastModifitactionString = ""
        if(years >0)
            lastModifitactionString = "$years years"
        else if(months>0)
            lastModifitactionString = "$months months"
        else if(days>0)
            lastModifitactionString = "$days days"
        else if(hours>0)
            lastModifitactionString = "$hours hours"
        else if(minutes>0)
            lastModifitactionString = "$minutes minutes"
        else
            lastModifitactionString = "$seconds seconds"
        return lastModifitactionString
    }
}
