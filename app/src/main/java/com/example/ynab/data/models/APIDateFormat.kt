package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class APIDateFormat(@SerializedName("format") var format: String)