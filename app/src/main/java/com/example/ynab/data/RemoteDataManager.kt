package com.example.ynab.data

import com.example.ynab.data.error.NETWORK_ERROR
import com.example.ynab.data.error.NO_INTERNET_CONNECTION
import com.example.ynab.data.models.*
import com.example.ynab.ui.utils.NetworkConnectivity
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataManager @Inject constructor(
    private val apiService: APIService,

    private val networkConnectivity: NetworkConnectivity
) : RemoteDataSource {
    private var createdAPIService: YNABNetworkServices =
        apiService.createService(YNABNetworkServices::class.java)

    private suspend fun processCall(

        responseCall: suspend () -> Response<*>
    ): Any? {

        if (!networkConnectivity.isConnected()) {
            return NO_INTERNET_CONNECTION
        }
        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                responseCode
            }
        } catch (e: IOException) {
            NETWORK_ERROR
        }
    }

    override suspend fun getBudgets(): Resource<BudgetsData> {
        return when (val response = processCall { createdAPIService.getBudgets() }) {
            is BudgetsResponse -> {
                Resource.Success(response.data)
            }
            else ->
                Resource.DataError(errorCode = response as Int)
        }
    }

    override suspend fun getBudgetAccounts(budgetId: String): Resource<AccountsData> {
        return when (val response = processCall {
            createdAPIService.getBudgetAccounts(budgetId)
        }) {
            is AccountsResponse -> {
                Resource.Success(response.data)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }


    override suspend fun addAccount(
        budgetId: String,
        createAccountEntity: CreateAccountRequestWrapper
    ): Resource<AddAccountResponse> {

        return when (val response = processCall {
            suspend {
                createdAPIService.createAccount(
                    budgetId,
                    createAccountEntity
                )
            }.invoke()
        }) {
            is AddAccountResponse -> {
                Resource.Success(response)
            }
            else -> {
                Resource.DataError(1)
            }
        }
    }
}