package com.example.ynab.data.repos

import com.example.ynab.data.RemoteDataManager
import com.example.ynab.data.Resource
import com.example.ynab.data.models.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class AccountRepository @Inject constructor(
    private val remoteDataManager: RemoteDataManager,
    private val ioDispatcher: CoroutineContext
) : AccountRepositorySource {
    override suspend fun getBudgetAccounts(budget: Budget): Flow<Resource<ArrayList<Account>>> {

        return flow {
            val status = remoteDataManager.getBudgetAccounts(budget.id)
            emit(when (status) {
                is Resource.Success -> {
                    val accounts = ArrayList<Account>()
                    status.data?.accounts?.forEach {
                        if (!it.deleted) {
                            val account = getAccountObject(it, budget)
                            accounts.add(account)
                        }
                    }
                    Resource.Success<ArrayList<Account>>(accounts)
                }

                else -> {
                    Resource.DataError<ArrayList<Account>>(1)
                }
            })
        }.flowOn(ioDispatcher)
    }

    override suspend fun createNewAccount(
        budget: Budget,
        account: NewAccount
    ): Flow<Resource<Account>?> {
        return flow {
            val createAccountRequestWrapper = getNewAccountRequestWrapper(account)
            val status = remoteDataManager.addAccount(budget.id, createAccountRequestWrapper)
            emit(when (status) {
                is Resource.Success -> {
                    if (status.data?.addAccountData?.account != null) {
                        val account =
                            status.data?.addAccountData?.account?.let {
                                getAccountObject(
                                    it,
                                    budget
                                )
                            }
                        Resource.Success<Account>(account)
                    } else {
                        Resource.DataError<Account>(1)
                    }
                }
                else -> {
                    status.errorCode?.let { Resource.DataError<Account>(it) }
                }
            })

        }.flowOn(ioDispatcher)
    }

    private fun getNewAccountRequestWrapper(account: NewAccount): CreateAccountRequestWrapper {
        val accountRequestWrapper = CreateAccountRequestWrapper(
            AccountRequestEnitity(
                name = account.name,
                type = account.type,
                balance = (account.balance * 1000).toInt()
            )
        )
        return accountRequestWrapper
    }

    private fun getAccountObject(accountEntity: AccountEntity, budget: Budget): Account {
        val balance = beautifyCurrencyAmount(
            decimalSeparator = budget.currencyFormat.decimalSeparator,
            amount = accountEntity.balance,
            currencySymbol = budget.currencyFormat.currencySymbol,
            displaySymbol = budget.currencyFormat.displaySymbol,
            numberOfDecimalPoints = budget.currencyFormat.dicimalDigits,
            symbolFirst = budget.currencyFormat.symbolFirst
        )
        val clearedBalance = beautifyCurrencyAmount(
            decimalSeparator = budget.currencyFormat.decimalSeparator,
            amount = accountEntity.clearedBalance,
            currencySymbol = budget.currencyFormat.currencySymbol,
            displaySymbol = budget.currencyFormat.displaySymbol,
            numberOfDecimalPoints = budget.currencyFormat.dicimalDigits,
            symbolFirst = budget.currencyFormat.symbolFirst
        )
        val unclearedBalance = beautifyCurrencyAmount(
            decimalSeparator = budget.currencyFormat.decimalSeparator,
            amount = accountEntity.unclearedBalance,
            currencySymbol = budget.currencyFormat.currencySymbol,
            displaySymbol = budget.currencyFormat.displaySymbol,
            numberOfDecimalPoints = budget.currencyFormat.dicimalDigits,
            symbolFirst = budget.currencyFormat.symbolFirst
        )
        val account = Account(
            id = accountEntity.id,
            closed = accountEntity.closed,
            name = accountEntity.name,
            note = accountEntity.note,
            on_budget = accountEntity.on_budget,
            type = accountEntity.type,
            transeferPayeeId = accountEntity.transeferPayeeId,
            beautifiedClearedBalance = clearedBalance,
            beautifiedBalance = balance,
            beautifiedUnclearedBalance = unclearedBalance
        )
        return account
    }

    private fun beautifyCurrencyAmount(
        decimalSeparator: String,
        amount: Double,
        numberOfDecimalPoints: Int,
        symbolFirst: Boolean,
        displaySymbol: Boolean,
        currencySymbol: String

    ): String {
        val number: Int = (amount / 1000).toInt()

        // val decimal: Int = (* 1000).toInt()
        val decimal = ((amount / 1000) - number)
        var decimalTrimmed = ""
        if (decimal > 0)
            decimalTrimmed =
                ((String.format("%.${numberOfDecimalPoints}f", decimal).toDouble()) * 1000).toInt()
                    .toString()
        else
            decimalTrimmed = "0".repeat(numberOfDecimalPoints)


        var beatifiedAmount = ""
        if (displaySymbol)
            if (symbolFirst)
                beatifiedAmount = "${currencySymbol}${number}${decimalSeparator}${decimalTrimmed}"
            else
                beatifiedAmount = "${number}${decimalSeparator}${decimalTrimmed}${currencySymbol}"
        else
            beatifiedAmount = "${number}${decimalSeparator}${decimalTrimmed}"
        return beatifiedAmount
    }
}