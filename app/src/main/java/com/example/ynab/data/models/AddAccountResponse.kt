package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class AddAccountResponse(@SerializedName("data")var addAccountData:AddAccountData) {
}