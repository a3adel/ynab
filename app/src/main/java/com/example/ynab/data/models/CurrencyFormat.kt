package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class CurrencyFormat(
    @SerializedName("iso_code") var isoCode: String,
    @SerializedName("example_format")var exampleFormat:String,
    @SerializedName("decimal_digits")var dicimalDigits:Int,
    @SerializedName("decimal_separator")var decimalSeparator:String,
    @SerializedName("symbol_first")var symbolFirst:Boolean,
    @SerializedName("group_separator")var groupSeparator:String,
    @SerializedName("currency_symbol")var currencySymbol:String,
    @SerializedName("display_symbol")var displaySymbol:Boolean
) {

}
