package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class AccountsResponse(
    @SerializedName("data") var data: AccountsData,
    @SerializedName("server_knowledge") var serverKnowledge: Int
)