package com.example.ynab.data.repos

import com.example.ynab.data.Resource
import com.example.ynab.data.models.AccountsData
import com.example.ynab.data.models.BudgetsData
import kotlinx.coroutines.flow.Flow

interface BudgetRepositorySource {
    suspend fun getBudgets(): Flow<Resource<BudgetsData>>
}