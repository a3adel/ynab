package com.example.ynab.data.repos


import com.example.ynab.data.RemoteDataManager
import com.example.ynab.data.Resource
import com.example.ynab.data.models.AccountsData
import com.example.ynab.data.models.BudgetsData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class BudgetRepository @Inject constructor(
    private val remoteDataManager: RemoteDataManager,
    private val ioDispatcher: CoroutineContext
) : BudgetRepositorySource {
    override suspend fun getBudgets(): Flow<Resource<BudgetsData>> {
        return flow { emit(remoteDataManager.getBudgets()) }.flowOn(ioDispatcher)
    }


}