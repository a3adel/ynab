package com.example.ynab.data.models

import com.google.gson.annotations.SerializedName

data class AccountEntity(
    @SerializedName("id") var id: String,
    @SerializedName("name") var name: String,
    @SerializedName("type") var type: String,
    @SerializedName("on_budget") var on_budget: Boolean,
    @SerializedName("closed") var closed: Boolean,
    @SerializedName("balance") var balance: Double,
    @SerializedName("note") var note: String? = null,
    @SerializedName("cleared_balance") var clearedBalance: Double,
    @SerializedName("uncleared_balance") var unclearedBalance: Double,
    @SerializedName("transfer_payee_id") var transeferPayeeId: String,
    @SerializedName("deleted") var deleted: Boolean

)