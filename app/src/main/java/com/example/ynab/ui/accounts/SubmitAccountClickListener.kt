package com.example.ynab.ui.accounts

import android.accounts.Account
import com.example.ynab.data.models.NewAccount

interface SubmitAccountClickListener {
    fun onAccountSubmitted(account:NewAccount)
}