package com.example.ynab.ui.utils

interface OnRecyclerViewItemClickListener {
    fun onItemClicked(position: Int)
}