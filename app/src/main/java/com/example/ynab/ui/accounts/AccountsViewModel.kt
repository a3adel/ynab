package com.example.ynab.ui.accounts

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.themoviesdb.ui.base.BaseViewModel
import com.example.ynab.data.Resource
import com.example.ynab.data.models.Account
import com.example.ynab.data.models.Budget
import com.example.ynab.data.models.NewAccount
import com.example.ynab.data.repos.AccountRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountsViewModel @Inject constructor(private val dataRepo: AccountRepository) :
    BaseViewModel() {
    private val accountsLiveDataPrivate = MutableLiveData<ArrayList<Account>>()
    val accountsLiveData: LiveData<ArrayList<Account>> get() = accountsLiveDataPrivate


    private val errorLiveDataPrivate = MutableLiveData<Int>()
    val errorLiveData: LiveData<Int> get() = errorLiveDataPrivate

    private val newAccountLiveDataPrivate = MutableLiveData<Resource<Account>>()
    val newAccountLiveData: LiveData<Resource<Account>> get() = newAccountLiveDataPrivate
    @SuppressLint("NullSafeMutableLiveData")
    fun getBudgetAccounts(budget: Budget) {
        viewModelScope.launch {
            dataRepo.getBudgetAccounts(budget).collect {
                when (it) {
                    is Resource.Success -> {
                        accountsLiveDataPrivate.value = it.data
                    }
                    else -> {
                        if (it != null && it.errorCode != null)
                            errorLiveDataPrivate.value = it.errorCode!!
                        else
                            errorLiveDataPrivate.value = 1
                    }
                }

            }
        }
    }

    fun addAccount(budget: Budget, account: NewAccount) {
        viewModelScope.launch {
            dataRepo.createNewAccount(budget, account).collect {
                newAccountLiveDataPrivate.value = it

            }
        }
    }

    fun updateAccountsList(account: Account?) {
        account?.let {
            accountsLiveDataPrivate.value?.add(it)
            accountsLiveDataPrivate.value = accountsLiveDataPrivate.value
        }
    }


}