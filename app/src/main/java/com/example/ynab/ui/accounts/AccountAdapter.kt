package com.example.ynab.ui.accounts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ynab.R
import com.example.ynab.data.models.Account
import kotlinx.android.synthetic.main.item_account.view.*

class AccountAdapter : RecyclerView.Adapter<AccountAdapter.AccountViewHolder>() {
    val accounts = ArrayList<Account>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_account, parent, false)
        return AccountViewHolder(view)
    }

    override fun getItemCount(): Int {
        return accounts.size
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.itemView.accountNameTextView.text = accounts.get(position).name
        holder.itemView.balanceTextView.text = accounts.get(position).beautifiedBalance
        holder.itemView.clearedBalanceTextView.text =
            accounts.get(position).beautifiedClearedBalance
        holder.itemView.unClearedBalanceTextView.text =
            accounts.get(position).beautifiedUnclearedBalance
        holder.itemView.typeTextView.text = accounts.get(position).type
    }

    class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}


