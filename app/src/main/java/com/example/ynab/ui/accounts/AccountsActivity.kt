package com.example.ynab.ui.accounts

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesdb.ViewModelFactory
import com.example.themoviesdb.ui.base.BaseActivity
import com.example.ynab.R
import com.example.ynab.data.Resource
import com.example.ynab.data.models.Account
import com.example.ynab.data.models.Budget
import com.example.ynab.data.models.NewAccount
import com.example.ynab.ui.utils.observe
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_accounts.*
import javax.inject.Inject

const val BUDGET_KEY = "budget"

class AccountsActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var viewModel: AccountsViewModel
    val adapter = AccountAdapter()
    lateinit var budget: Budget
    override fun initializeViewModel() {
        viewModel = viewModelFactory.create(AccountsViewModel::class.java)
    }

    override fun observeViewModel() {
        observe(viewModel.accountsLiveData, ::handleAccountsResponse)
        observe(viewModel.newAccountLiveData, ::handleNewAccountResponse)
        observe(viewModel.errorLiveData, ::handleError)
    }

    private fun handleNewAccountResponse(resource: Resource<Account>) {
        when (resource) {
            is Resource.Success -> {
                viewModel.updateAccountsList(resource.data)
            }
            else -> {
                showToast(getString(R.string.error))
                accountsProgressbar.visibility = View.GONE

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts)
        val budgetString = intent.getStringExtra(BUDGET_KEY)
        budget = Gson().fromJson(budgetString, Budget::class.java)
        initViews()
        viewModel.getBudgetAccounts(budget)
    }

    fun handleAccountsResponse(response: ArrayList<Account>) {
        adapter.accounts.clear()
        response?.let {
            adapter.accounts.addAll(it)
            adapter.notifyDataSetChanged()
        }

        accountsProgressbar.visibility = View.GONE

    }

    fun handleError(errorCode: Int) {
        Toast.makeText(this, "error", Toast.LENGTH_LONG).show()

    }

    fun initViews() {
        val layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(
            accountsRecyclerView.context,
            layoutManager.orientation
        )
        accountsRecyclerView.layoutManager = layoutManager
        accountsRecyclerView.addItemDecoration(dividerItemDecoration)
        accountsRecyclerView.adapter = adapter
        addAccountFAB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                val addAccountDialog = CreateAccountDialog()
                addAccountDialog.show(supportFragmentManager, "asd")
                addAccountDialog.submitAccountClickListener = object : SubmitAccountClickListener {
                    override fun onAccountSubmitted(account: NewAccount) {

                        viewModel.addAccount(budget = budget, account = account)
                        accountsProgressbar.visibility = View.VISIBLE
                    }
                }
            }
        })
    }
}