package com.example.ynab.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.themoviesdb.ui.base.BaseViewModel
import com.example.ynab.data.repos.BudgetRepository
import com.example.ynab.data.Resource
import com.example.ynab.data.models.BudgetsData
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val dataManager: BudgetRepository) : BaseViewModel() {
    private val budgetsLiveDataPrivate = MutableLiveData<Resource<BudgetsData>>()
    val budgetsLiveData: LiveData<Resource<BudgetsData>> get() = budgetsLiveDataPrivate
    fun getBudgets() {
        viewModelScope.launch {
            budgetsLiveDataPrivate.value = Resource.Loading()
            dataManager.getBudgets().collect {
                budgetsLiveDataPrivate.value = it
            }
        }
    }
}