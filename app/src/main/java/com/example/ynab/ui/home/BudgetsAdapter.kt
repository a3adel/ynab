package com.example.ynab.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ynab.R
import com.example.ynab.data.models.Budget
import com.example.ynab.ui.utils.OnRecyclerViewItemClickListener
import kotlinx.android.synthetic.main.item_budget.view.*

class BudgetsAdapter : RecyclerView.Adapter<BudgetsAdapter.BudgetViewHolder>() {
    val budgets: ArrayList<Budget> = ArrayList()
    lateinit var onRecyclerViewItemClickListener: OnRecyclerViewItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BudgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_budget, parent, false)
        return BudgetViewHolder(view)
    }

    override fun getItemCount(): Int {
        return budgets.size
    }

    override fun onBindViewHolder(holder: BudgetViewHolder, position: Int) {
        holder.itemView.budgetNameTextView.text = "name:- ${budgets.get(position).name}"
        holder.itemView.currencyTextView.text =
            "Currency:- ${budgets.get(position).currencyFormat.isoCode}"
        holder.itemView.lastModificationDateTextView.text =
            "last modified from ${budgets.get(position).getLastModified()}"
        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                onRecyclerViewItemClickListener.onItemClicked(position)
            }
        })
    }

    class BudgetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}