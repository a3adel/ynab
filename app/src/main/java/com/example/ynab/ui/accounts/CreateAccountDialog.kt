package com.example.ynab.ui.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.ynab.R
import com.example.ynab.data.models.NewAccount
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_create_account.*
import kotlinx.android.synthetic.main.dialog_create_account.view.*

class CreateAccountDialog : BottomSheetDialogFragment() {
    lateinit var submitAccountClickListener: SubmitAccountClickListener
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_create_account, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.types,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                view.accountTypeSpinner.adapter = adapter
            }
        }
        submitButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                val name = accountNameEditText.text.toString()
                val balance = accountBalanceEditText.text.toString()

                if (name.isEmpty() or balance.isEmpty()) {
                    Toast.makeText(
                        context,
                        getString(R.string.error_add_account),
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    val type = accountTypeSpinner.selectedItem.toString()
                    val newAccount =
                        NewAccount(name = name, balance = balance.toDouble(), type = type)
                    submitAccountClickListener.onAccountSubmitted(newAccount)
                    dismiss()
                }
            }

        })
    }

}