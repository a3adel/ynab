package com.example.ynab.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesdb.ViewModelFactory
import com.example.themoviesdb.ui.base.BaseActivity
import com.example.ynab.R
import com.example.ynab.data.Resource
import com.example.ynab.data.models.BudgetsData
import com.example.ynab.ui.accounts.AccountsActivity
import com.example.ynab.ui.accounts.BUDGET_KEY
import com.example.ynab.ui.utils.OnRecyclerViewItemClickListener
import com.example.ynab.ui.utils.observe
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class HomeActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var viewModel: HomeViewModel

    lateinit var adapter: BudgetsAdapter
    override fun initializeViewModel() {
        viewModel = viewModelFactory.create(HomeViewModel::class.java)
    }

    override fun observeViewModel() {
        observe(viewModel.budgetsLiveData, ::handleBudgetsResponse)
    }

    private fun handleBudgetsResponse(status: Resource<BudgetsData>) {
        when (status) {
            is Resource.Success -> {
                adapter.budgets.clear()
                status?.data?.budgets?.let {
                    adapter.budgets.addAll(it)
                    adapter.notifyDataSetChanged()
                    adapter.onRecyclerViewItemClickListener =
                        object : OnRecyclerViewItemClickListener {
                            override fun onItemClicked(position: Int) {
                                val intent = Intent(this@HomeActivity, AccountsActivity::class.java)
                                val budgetString = Gson().toJson(it.get(position))
                                intent.putExtra(BUDGET_KEY, budgetString)
                                startActivity(intent)
                            }
                        }
                }
                budgetsProgressBar.visibility = GONE

            }
            is Resource.DataError -> {
                Toast.makeText(this, "" + status.errorCode, Toast.LENGTH_LONG)
                    .show()
                budgetsProgressBar.visibility = GONE

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        budgetsProgressBar.visibility = VISIBLE
        viewModel.getBudgets()

    }

    private fun initViews() {
        adapter = BudgetsAdapter()
        val layoutManager = LinearLayoutManager(this)
        budgetsRecyclerView.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(
            budgetsRecyclerView.getContext(),
            layoutManager.orientation
        )

        budgetsRecyclerView.addItemDecoration(dividerItemDecoration)
        budgetsRecyclerView.adapter = adapter

    }
}