package com.example.themoviesdb.di.modules

import android.app.Application
import android.content.Context
import com.example.ynab.YNABApp
import com.example.ynab.ui.utils.NetworkConnectivity
import com.example.ynab.ui.utils.NetworkUtils

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

@Module
class ApplicationModule() {
    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.IO
    }

    @Provides
    @Singleton
    fun provideNetworkConnectivity(): NetworkConnectivity {
        return NetworkUtils(YNABApp.context)
    }

}