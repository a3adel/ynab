package com.example.ynab.di.modules


import com.example.ynab.data.repos.AccountRepository
import com.example.ynab.data.repos.AccountRepositorySource
import com.example.ynab.data.repos.BudgetRepository
import com.example.ynab.data.repos.BudgetRepositorySource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {
    @Binds
    @Singleton
    abstract fun provideDataRepo(appDataManager: BudgetRepository): BudgetRepositorySource

    @Binds
    @Singleton
    abstract fun provideAccountsRepoRepo(appDataManager: AccountRepository): AccountRepositorySource
}