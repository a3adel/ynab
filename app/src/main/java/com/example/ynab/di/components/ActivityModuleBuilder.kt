package com.example.ynab.di.components


import com.example.ynab.ui.accounts.AccountsActivity
import com.example.ynab.ui.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
@Module
abstract class ActivityModuleBuilder {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun contributeAccountsActivity():AccountsActivity
}